package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MainGenerator {
    private static final String PROJECT_DIR = System.getProperty("user.dir");
    public static void main(String[] args) {
        Schema schema = new Schema(1, "com.casabio.Model");
        schema.enableKeepSectionsByDefault();
        addTables(schema);
        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR + "\\app\\src\\main\\java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void addTables(final Schema schema) {
        Entity userModel = addUser(schema);
        Entity repo = addRepo(schema);
        Entity collectionModel = addCollection(schema);
        Entity observationModel = addObservation(schema);
        Entity taxonomyModel = addTaxonomy(schema);
        Entity identificationModel = addIdentification(schema);
        Entity genericModel = addGeneric(schema);

        Property userId = repo.addLongProperty("userId").notNull().getProperty();
        userModel.addToMany(repo, userId, "userRepos");

    }
    private static Entity addUser(final Schema schema) {

        //[{"uid":"296","name":"Test admin","theme":"","signature":"","signature_format":"raw_html",
        // "created":"1455372083","access":"1455526822","login":"1455374196","status":"1","timezone":"Africa/Johannesburg",
        // "language":"","picture":"0","data":"a:2:{s:7:\"contact\";i:1;s:7:\"overlay\";i:1;}","uri":"http://stage.touchdreams.co.za/CsB/api/v1.0/users/296"}

        Entity user = schema.addEntity("UserEntity");
        user.addStringProperty("uid").primaryKey();
        user.addStringProperty("name");
        user.addStringProperty("password");
        user.addStringProperty("mail");
        user.addStringProperty("theme");
        user.addStringProperty("signature");
        user.addStringProperty("signature_format");
        user.addStringProperty("created");
        user.addStringProperty("access");
        user.addStringProperty("login");
        user.addStringProperty("timezone");
        user.addStringProperty("language");
        user.addStringProperty("picture");
        user.addStringProperty("init");
        user.addStringProperty("data");
        user.addStringProperty("uri");
        return user;
    }
    private static Entity addCollection(final Schema schema) {
        //{"nid":"63640","vid":"63640","type":"collection","language":"und",
        // "title":"Super Stuff","uid":"2","status":"1","created":"1457974562",
        // "changed":"1457974567","comment":"1","promote":"0","sticky":"0",
        // "tnid":"0","translate":"0","uri":"http://stage.touchdreams.co.za/CsB/api/v1.0/node/63640"}
        Entity collection= schema.addEntity("CollectionEntity");
        collection.addStringProperty("nid").primaryKey();
        collection.addStringProperty("vid");
        collection.addStringProperty("type");
        collection.addStringProperty("language");
        collection.addStringProperty("title");
        collection.addStringProperty("uid");
        collection.addStringProperty("status");
        collection.addStringProperty("created");
        collection.addStringProperty("changed");
        collection.addStringProperty("comment");
        collection.addStringProperty("promote");
        collection.addStringProperty("sticky");
        collection.addStringProperty("tnid");
        collection.addStringProperty("translate");
        collection.addStringProperty("uri");
        return collection;
    }
    private static Entity addObservation(final Schema schema) {
        //[{"nid":"1086","vid":"1086","type":"observation","language":"und","title":"PreCIS-461959451",
        // "uid":"1","status":"1","created":"1447160500","changed":"1456306489",
        // "comment":"2","promote":"1","sticky":"0","tnid":"0","translate":"0",
        // "uri":"http://stage.touchdreams.co.za/CsB/api/v1.0/node/1086"}
        Entity observation = schema.addEntity("ObservationEntity");
        observation.addStringProperty("nid").primaryKey();
        observation.addStringProperty("vid");
        observation.addStringProperty("type");
        observation.addStringProperty("language");
        observation.addStringProperty("title");
        observation.addStringProperty("uid");
        observation.addStringProperty("status");
        observation.addStringProperty("created");
        observation.addStringProperty("changed");
        observation.addStringProperty("comment");
        observation.addStringProperty("promote");
        observation.addStringProperty("sticky");
        observation.addStringProperty("tnid");
        observation.addStringProperty("translate");
        observation.addStringProperty("uri");
        observation.addByteArrayProperty("image");
        observation.addStringProperty("image_path");
        return observation;
    }
    private static Entity addTaxonomy(final Schema schema) {
        //{"tid":"88195","vid":"2","name":"Zyrphelis taxifolia",
        // "description":"Slender, sparsely hairy, sprawling subshrub to 40 cm. Leaves linear,
        // apiculate, the margins minutely toothed. Flower heads radiate, solitary on long peduncles, blue or mauve with yellow disc.
        // Mainly Sept.--Dec. Damp sandstone slopes, SW (Cape Peninsula to Cape Hangklip).",
        // "format":"full_html","weight":"0","uuid":"","parent":"103910","uri":"http://stage.touchdreams.co.za/CsB/api/v1.0/taxonomy_term/88195"}
        Entity taxa = schema.addEntity("TaxaEntity");
        taxa.addStringProperty("tid").primaryKey();
        taxa.addStringProperty("vid");
        taxa.addStringProperty("name");
        taxa.addStringProperty("description");
        taxa.addStringProperty("format");
        taxa.addStringProperty("weight");
        taxa.addStringProperty("uuid");
        taxa.addStringProperty("parent");
        taxa.addStringProperty("uri");
        return taxa;
    }
    private static Entity addIdentification(final Schema schema) {
        Entity identification = schema.addEntity("IdentificationEntity");
        identification.addStringProperty("nid").primaryKey();//.autoincrement();
        identification.addStringProperty("name").notNull();
        identification.addShortProperty("age");
        return identification;
    }
    private static Entity addGeneric(final Schema schema) {
        Entity generic = schema.addEntity("GenericEntity");
        generic.addStringProperty("nid").primaryKey();//.autoincrement();
        generic.addStringProperty("name").notNull();
        generic.addShortProperty("age");
        return generic;
    }
    private static Entity addRepo(final Schema schema) {
        Entity repo = schema.addEntity("Repo");
        repo.addStringProperty("repo").primaryKey();
        repo.addStringProperty("title").notNull();
        repo.addStringProperty("language");
        repo.addIntProperty("watchers_count");
        return repo;
    }

}