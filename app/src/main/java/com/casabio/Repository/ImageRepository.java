package com.casabio.Repository;

import android.content.Context;

import java.util.List;

/**
 * Created by OMBMAR001 on 2016/04/20.
 */
public class ImageRepository extends AbstractRepository {

    public ImageRepository(Context context) {
        super(context);
    }

    @Override
    public boolean addEntityDB(Object o) {
        return false;
    }

    @Override
    public boolean deleteEntityDB(String id) {
        return false;
    }

    @Override
    public boolean editEntityDB(String id, Object obj) {
        return false;
    }

    @Override
    public Object getEntityDB(String id) {
        return null;
    }

    @Override
    public Object getAllDB() {
        return null;
    }

    @Override
    public boolean addEntityAPI(Object obj) {
        return false;
    }

    @Override
    public boolean deleteEntityAPI(String id) {
        return false;
    }

    @Override
    public boolean editEntityAPI(String id, Object obj) {
        return false;
    }

    @Override
    public Object getEntityAPI(String id) {
        return null;
    }

    @Override
    public List getAllAPI() {
        return null;
    }


}
