package com.casabio.Repository;

import com.casabio.Model.ObservationEntity;
import com.casabio.Model.UserEntity;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by OMBMAR001 on 2016/04/26.
 */
public interface API {
    public static String BASE_URL = "http://stage.touchdreams.co.za";

    //-------------------------------------Observation
    @GET("/CsB/api/v1.0/observations")
    Call<List<ObservationEntity>> getObservations();

    @POST("/CsB/api/v1.0/observations")
    Call addObservation();

    @GET("/CsB/api/v1.0/observations")
    Call<ObservationEntity> getObservationByUId(@Query("sort") String sort);

    @GET("/CsB/api/v1.0/observations/{id}")
    Call<ObservationEntity> getObservationById(@Path("id") int id);


    //-------------------------------------User
    @GET("/CsB/api/v1.0/users")
    Call<UserEntity> getUsers();

    @POST("/CsB/api/v1.0/users")
    Call<UserEntity> addUser();

    @DELETE("/CsB/api/v1.0/users/{uid}")
    Call<UserEntity> deleteUser(@Path("uid") int uid);

    @GET("/CsB/api/v1.0/users/{uid}")
    Call<UserEntity> getUserById(@Path("uid") int uid);

    @PUT("/CsB/api/v1.0/users/{uid}")
    Call<UserEntity> updateUser(@Path("uid") int uid);

    //-------------------------------------Identification
//    @GET("/CsB/api/v1.0/users")
//    Call<UserEntity> getUsers();
//
//    @POST("/CsB/api/v1.0/users")
//    Call<UserEntity> addUser();
//
//    @DELETE("/CsB/api/v1.0/users/{uid}")
//    Call<UserEntity> deleteUser();
//
//    @GET("/CsB/api/v1.0/users")
//    Call<UserEntity> getUserById();
//
//    @PUT("/CsB/api/v1.0/users/{uid}")
//    Call<UserEntity> updateUser();


}
