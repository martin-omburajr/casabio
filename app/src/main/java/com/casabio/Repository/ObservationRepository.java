package com.casabio.Repository;

import android.content.Context;

import com.casabio.Model.ObservationEntity;
import com.casabio.Model.ObservationEntityDao;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by OMBMAR001 on 2016/04/20.
 */
public class ObservationRepository extends AbstractRepository {

    private ObservationEntityDao observationEntityDao = daoSession.getObservationEntityDao();
    private Retrofit retrofit;
    static List<ObservationEntity> observationEntities;
    static ObservationEntity observationEntity;

    public ObservationRepository(Context context) {
        super(context);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://stage.touchdreams.co.za")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.retrofit = retrofit;
    }


    @Override
    public boolean addEntityDB(Object o) {
        ObservationEntity e;
        try {
            e = (ObservationEntity)o;
            observationEntityDao.insert(e);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteEntityDB(String newId) {
        //long newId = (long)id;
        try {
            ObservationEntity observationEntity = observationEntityDao.load(newId);
            observationEntityDao.deleteByKey(newId);
            return true;
        }catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean editEntityDB(String id, Object obj) {

        ObservationEntity o;
        try {
            o = (ObservationEntity)obj;
            observationEntityDao.update(o);
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }

    @Override
    public Object getEntityDB(String newId) {
        //long newId = (long)id;
        try {
            ObservationEntity observationEntity = observationEntityDao.load(newId);
            return observationEntity;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getAllDB() {
        return null;
    }

    @Override
    public boolean addEntityAPI(Object obj) {
        ObservationEntity entity;
        try{
            entity = (ObservationEntity)obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        //push to API
        return false;
    }

    @Override
    public boolean deleteEntityAPI(String id) {



        return false;
    }

    @Override
    public boolean editEntityAPI(String id, Object obj) {
        return false;
    }

    @Override
    public Object getEntityAPI(String id) {
        API API = retrofit.create(API.class);
        Call<ObservationEntity> call = API.getObservationById(Integer.parseInt(id));

        call.enqueue(new Callback<ObservationEntity>() {
            @Override
            public void onResponse(Call<ObservationEntity> call, Response<ObservationEntity> response) {
                observationEntity = response.body();
            }

            @Override
            public void onFailure(Call<ObservationEntity> call, Throwable t) {

            }
        });
        return observationEntity;
    }

    @Override
    public List getAllAPI() {
        API API = retrofit.create(API.class);
        Call<List<ObservationEntity>> call = API.getObservations();

        call.enqueue(new Callback<List<ObservationEntity>>() {
            @Override
            public void onResponse(Call<List<ObservationEntity>> call, Response<List<ObservationEntity>> response) {
                boolean isSuccesful = response.isSuccessful();
                observationEntities = response.body();
            }

            @Override
            public void onFailure(Call<List<ObservationEntity>> call, Throwable t) {
            }
        });
        return observationEntities;
}

}