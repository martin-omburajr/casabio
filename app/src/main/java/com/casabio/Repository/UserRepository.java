package com.casabio.Repository;

import android.content.Context;

import com.casabio.Model.UserEntity;
import com.casabio.Model.UserEntityDao;

import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by OMBMAR001 on 2016/03/18.
 */
public class UserRepository extends AbstractRepository
{

    UserEntityDao userEntityDao = daoSession.getUserEntityDao();
    static List<UserEntity> allUsers;

    public UserRepository(Context context) {
        super(context);
    }

    public List<UserEntity> retrieveAllFromDB()
    {
        List<UserEntity> users = userEntityDao.loadAll();
        allUsers = users;
        return users;
    }

    //API
    public List<UserEntity> getSignedUpUserFromAPI()
    {
        return null;
    }


    public boolean addUser(UserEntity userEntity) {
        try{
            userEntityDao.insert(userEntity);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public UserEntity getByEmailAddress(String email)
    {
        QueryBuilder qb = userEntityDao.queryBuilder();
        qb.where(UserEntityDao.Properties.Mail.eq(email));
        List user = qb.list();
        UserEntity user1 = (UserEntity) user.get(0);
        return user1;
    }

    @Override
    public boolean addEntityDB(Object obj) {
        UserEntity user;
        try {
            user = (UserEntity)obj;
            try {
                userEntityDao.insert(user);
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean deleteEntityDB(String newId) {
        try {
            UserEntity userEntity = userEntityDao.load(newId);
            userEntityDao.deleteByKey(newId);
            return true;
        }catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean editEntityDB(String newId, Object obj) {
        //long newId = (long)id;
        try {
            UserEntity userEntity = userEntityDao.load(newId);
            userEntityDao.update(userEntity);
            return true;
        }catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public Object getEntityDB(String newId) {
        //long newId = (long)id;
        try {
            UserEntity userEntity = userEntityDao.load(newId);
            return userEntity;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getAllDB() {
        return null;
    }

    @Override
    public boolean addEntityAPI(Object obj) {
        return false;
    }

    @Override
    public boolean deleteEntityAPI(String id) {
        return false;
    }

    @Override
    public boolean editEntityAPI(String id, Object obj) {
        return false;
    }

    @Override
    public Object getEntityAPI(String id) {
        return false;
    }

    @Override
    public List getAllAPI() {
        return null;
    }
}
