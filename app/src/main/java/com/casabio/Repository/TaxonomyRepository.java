package com.casabio.Repository;

import android.content.Context;

import com.casabio.Model.TaxaEntity;
import com.casabio.Model.TaxaEntityDao;

import java.util.List;

/**
 * Created by OMBMAR001 on 2016/04/20.
 */
public class TaxonomyRepository extends AbstractRepository {

    TaxaEntityDao taxaEntityDao = daoSession.getTaxaEntityDao();

    public TaxonomyRepository(Context context) {
        super(context);
    }

    @Override
    public boolean addEntityDB(Object o) {
        TaxaEntity e;
        try {
            e = (TaxaEntity)o;
            taxaEntityDao.insert(e);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteEntityDB(String newId) {
        try {
            TaxaEntity taxaEntity = taxaEntityDao.load(newId);
            taxaEntityDao.deleteByKey(newId);
            return true;
        }catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        //return false;
    }

    @Override
    public boolean editEntityDB(String id, Object obj) {
        TaxaEntity e;
        try {
            e = (TaxaEntity)obj;
            taxaEntityDao.insert(e);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public Object getEntityDB(String newId) {
        //long newId = (long)id;
        try {
            TaxaEntity taxaEntity = taxaEntityDao.load(newId);
            taxaEntityDao.deleteByKey(newId);
            return taxaEntity;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    @Override
    public Object getAllDB() {
//        long newId = (long)id;
//        try {
//            TaxaEntity taxaEntity = taxaEntityDao.load(newId);
//            taxaEntityDao.deleteByKey(newId);
//            return taxaEntity;
//        }catch (Exception ex) {
//            ex.printStackTrace();
//
// return null; return null;
//        }
        return null;
    }

    @Override
    public boolean addEntityAPI(Object obj) {
        return false;
    }

    @Override
    public boolean deleteEntityAPI(String id) {
        return false;
    }

    @Override
    public boolean editEntityAPI(String id, Object obj) {
        return false;
    }

    @Override
    public Object getEntityAPI(String id) {
        return null;
    }

    @Override
    public List getAllAPI() {
        return null;
    }


}
