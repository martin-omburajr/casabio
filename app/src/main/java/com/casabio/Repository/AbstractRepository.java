package com.casabio.Repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.casabio.Model.DaoMaster;
import com.casabio.Model.DaoSession;

import java.util.List;

/**
 * Created by OMBMAR001 on 2016/03/18.
 */
public abstract class AbstractRepository<Dummy> {

    static DaoSession daoSession;
    static DaoMaster.DevOpenHelper daoMasterDevopenHelper;
    static DaoMaster daoMaster;
    static SQLiteDatabase sqLiteDatabase;
    Context context;

    public AbstractRepository(Context context) {
        this.context = context;
        setupDB();
    }

    public void setupDB()
    {
        daoMasterDevopenHelper = new DaoMaster.DevOpenHelper(context,"casabio.db", null);
        sqLiteDatabase = daoMasterDevopenHelper.getWritableDatabase();
        daoMaster = new DaoMaster(sqLiteDatabase);
        daoSession = daoMaster.newSession();
    }

    public abstract boolean addEntityDB(Dummy dummy);

    public abstract boolean deleteEntityDB(String id);

    public abstract boolean editEntityDB(String id, Dummy obj);

    public abstract Dummy getEntityDB(String id);

    public abstract Dummy getAllDB();

    public abstract boolean addEntityAPI(Dummy obj);

    public abstract boolean deleteEntityAPI(String id);

    public abstract boolean editEntityAPI(String id, Dummy obj);

    public abstract Dummy getEntityAPI(String id);

    public abstract List<Dummy> getAllAPI();

}
