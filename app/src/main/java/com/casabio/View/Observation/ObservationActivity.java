package com.casabio.View.Observation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.casabio.Model.ObservationEntity;
import com.casabio.R;
import com.casabio.Repository.ObservationRepository;
import com.casabio.View.Camera.PhotoIntentActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObservationActivity extends AppCompatActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener, Callback<ObservationEntity>{

    final Context context = this;
    private Button button;
    private EditText result;
    List<ObservationEntity> observationEntities = new ArrayList<ObservationEntity>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        result = (EditText)findViewById(R.id.observationNameBox);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // result.setText("WHATT");
                ObservationRepository observationRepository = new ObservationRepository(context);
                //observationEntities = observationRepository.getEntityAPI("1")
                //result.setText(observationEntities.size());
                openCreateNewObservation(view);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        });
    }


    public void openCamera(View view)
    {
        Intent openCamera = new Intent(this, PhotoIntentActivity.class);
        startActivity(openCamera);
    }

    public void openCreateObservationPrompt()
    {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.prompt_create_observation, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.observationNameBox);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                result.setText(userInput.getText());
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public static final String NAME = "OBSERVATIONUS!";

    public void openOngoingObservation(View view)
    {
        Intent openOngoingObservation = new Intent(this, OngoingObservationActivity.class);
        openOngoingObservation.putExtra(NAME, "OBSERVATION1");
        startActivity(openOngoingObservation);
    }


    private void consumeAPI(List<ObservationEntity> observationEntities)
    {
        //if(o)
    }


    public void getObservations()
    {

    }

    public void openCreateNewObservation(View view)
    {
        Intent openOngoingObservation = new Intent(this, CreateObservationActivity.class);
        openOngoingObservation.putExtra(NAME, "OBSERVATION1");
        startActivity(openOngoingObservation);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResponse(Call<ObservationEntity> call, Response<ObservationEntity> response) {

    }

    @Override
    public void onFailure(Call<ObservationEntity> call, Throwable t) {

    }
}
