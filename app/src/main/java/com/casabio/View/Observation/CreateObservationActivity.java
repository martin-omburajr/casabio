package com.casabio.View.Observation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.casabio.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CreateObservationActivity extends AppCompatActivity {

    private static Context context;
    static String name;
    static String description;

    @Bind(R.id.nameBoxCreateObservation) EditText nameBox;
    @Bind(R.id.descriptionBoxCreateObservation) EditText descriptionBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_observation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{
                    name = nameBox.getText().toString();
                    description = descriptionBox.getText().toString();
                }catch(Exception ex) {
                    name = "Name";
                    description = "Description";
                }

                openOngoingObservation(view);
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void openOngoingObservation(View view)
    {
        Intent openOngoingObservation = new Intent(this, OngoingObservationActivity.class);
        openOngoingObservation.putExtra(name, name);
        openOngoingObservation.putExtra(description,description);
        startActivity(openOngoingObservation);
    }


}
