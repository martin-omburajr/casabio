package com.casabio.View.Observation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.casabio.R;
import com.casabio.Service.Generic.PhotoService;
import com.casabio.View.Camera.PhotoIntentActivity;

import java.io.File;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OngoingObservationActivity extends AppCompatActivity {

    //public static String FILE_PATH = Environment.DIRECTORY_DCIM;// + "AlbumName";

    List<String>fileNames;
    @Bind(R.id.nameEditText) EditText nameBox;
    @Bind(R.id.descriptionEditText) EditText descriptionBox;
    @Bind(R.id.imageListView) ListView imageList;
    @Bind(R.id.takePhotoButton) Button takePhotoBtn;
    String name;
    String description;


    Integer[] imageId = {
            R.drawable.image1,
            R.drawable.image2

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ongoing_observation);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        name = intent.getStringExtra(CreateObservationActivity.name);
        description = intent.getStringExtra(CreateObservationActivity.description);
        imageList.setLongClickable(true);
        imageList.setClickable(true);

        File [] files = PhotoService.dcim.listFiles();
        fileNames = PhotoService.getFileNames(files);

        takePhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
            }
        });

        final View.OnLongClickListener listener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Snackbar.make(v, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return false;
            }
        };


        try {
           final String [] titles = PhotoService.toArrayString(fileNames);
            Bitmap [] bitmaps = PhotoService.toArray(PhotoService.getAllImages(fileNames, files));

            CustomList adapter = new
                    CustomList(OngoingObservationActivity.this, titles, imageId, bitmaps);
            //list=(ListView)findViewById(R.id.list);
            imageList.setAdapter(adapter);
            imageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    Toast.makeText(OngoingObservationActivity.this, "You Clicked at " +titles[+ position], Toast.LENGTH_SHORT).show();

                }
            });
        } catch(Exception e) {
            e.printStackTrace();
        }





        //Set EditBoxes
        nameBox.setText(name);
        descriptionBox.setText(description);
    }





    public void openCamera()
    {
        Intent takePictureIntent = new Intent(this, PhotoIntentActivity.class);
        startActivity(takePictureIntent);
    }
}
