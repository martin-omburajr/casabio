package com.casabio.View.Observation;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.casabio.R;

/**
 * Created by OMBMAR001 on 2016/05/08.
 */
public class ObservationFragment extends Fragment {

    public ObservationFragment()
    {

    }

    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        myView = inflater.inflate(R.layout.content_observation, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
