package com.casabio.View.Main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.casabio.R;
import com.casabio.View.Authentication.LoginActivity;
import com.casabio.View.Authentication.SignUpActivity;
import com.casabio.View.Camera.PhotoIntentActivity;
import com.casabio.View.Gallery.GridViewActivity;
import com.casabio.View.Observation.ObservationActivity;
import com.casabio.View.User.ProfileActivity;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //openObservation();
                //openSignUp();
                openGallery2();
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            openSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        android.app.FragmentManager fragmentManager = getFragmentManager();
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {

            this.openCamera();
        } else if (id == R.id.nav_observation) {
            this.openObservation();
        } else if (id == R.id.nav_gallery) {
            this.openGallery2();
        } else if (id == R.id.nav_profile) {
            this.openProfile();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    static final int REQUEST_IMAGE_CAPTURE = 1;

    public void openCamera()
    {
        Intent takePictureIntent = new Intent(this, PhotoIntentActivity.class);
        startActivity(takePictureIntent);
    }

    public void openObservation()
    {
        Intent openObservation = new Intent(this, ObservationActivity.class);
        startActivity(openObservation);
    }

    public void openSettings()
    {
        Intent openSettings = new Intent(this, LoginActivity.class);
        startActivity(openSettings);
    }

    public void openGallery()
    {
        Intent openPhotoGallery = new Intent(this, LoginActivity.class);
        startActivity(openPhotoGallery);
    }

    public void openProfile()
    {
        Intent openProfile = new Intent(this, ProfileActivity.class);
        startActivity(openProfile);
    }

    public void openSignUp()
    {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    public void openGallery2()
    {
        Intent intent = new Intent(this, GridViewActivity.class);
        startActivity(intent);
    }
}