package com.casabio.Service.Connection;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by OMBMAR001 on 2016/04/17.
 */
public class ConnectionService {

    public boolean isOnline(ConnectivityManager cm) {

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
}
