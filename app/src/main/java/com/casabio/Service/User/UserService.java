package com.casabio.Service.User;

import android.content.Context;

import com.casabio.Model.UserEntity;
import com.casabio.Repository.UserRepository;
import com.casabio.Service.AbstractService;

public class UserService extends AbstractService {
	
	public static final String TYPE = "USERSERVICE";
	public static final String USER_GET_URL = "http://stage.touchdreams.co.za/CsB/api/v1.0/users";
	UserRepository userRepository;

	public UserService(Context context)
	{
		userRepository =  new UserRepository(context);
	}
	
	public boolean ValidateLogin(String username, String password)
	{
		//Take data and send it to the Repository to validate then retrieve the data.

		return false;
	}
	
	public boolean SignUp(String userName, String name, String emailAddress, String password)
	{
		//Persist to API
		//User then has to login
		//Then retrieve user from web then add to DB.
		UserEntity userEntity = new UserEntity();
		return false;
	}

	public boolean SignUpToDB(String name, String emailAddress, String password)
	{
		UserEntity userEntity = new UserEntity();
		userEntity.setName(name);
		userEntity.setMail(emailAddress);
		userEntity.setPassword(password);
		try {
			userRepository.addEntityDB(userEntity);
			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public UserEntity getByEmailAddress(String emailAddress) {
		return userRepository.getByEmailAddress(emailAddress);
	}


	public boolean UserExistsInDB(String id){
		if(userRepository.getEntityDB(id) != null)
		{
			return true;
		} else {
			return false;
		}
	}


	public boolean DeleteAccount(int id){
		return false;
	}
	
	public boolean EditAccount(int id, Object [] attributes)
	{
		return false;
	}	

}
