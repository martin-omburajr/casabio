package com.casabio.Service.Generic;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by OMBMAR001 on 2016/03/20.
 */
public class PhotoService{

    public static String PATH = "";
    public static File rootsd = Environment.getExternalStorageDirectory();
    public static File dcim = new File(rootsd.getAbsolutePath() + "/Pictures/AlbumName");
    public List<String> photoPaths()
    {
        return null;
    }

    public static Bitmap getImageFromFile(File fullFileName)
    {

        Bitmap bitmap = BitmapFactory.decodeFile(fullFileName.getPath());
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 30,20,true);
        return newBitmap;
    }

    public static List<Bitmap> getAllImages(List<String> fileNames, File [] file)
    {
        List<Bitmap> bitmaps = new ArrayList<Bitmap>();
        for(int i = 0 ; i < fileNames.size(); i++)
        {
            Bitmap bitmap = PhotoService.getImageFromFile(file[i]);
                    bitmaps.add(bitmap);
        }
        return bitmaps;
    }

    public static Bitmap[] toArray(List<Bitmap> list)
    {
        Bitmap [] bitmaps = new Bitmap[list.size()];
        for(int i = 0; i < list.size(); i++) {
            bitmaps[i] = list.get(i);
        }
        return bitmaps;
    }

    public static String[] toArrayString(List<String> list)
    {
        String [] bitmaps = new String[list.size()];
        for(int i = 0; i < list.size(); i++) {
            bitmaps[i] = list.get(i);
        }
        return bitmaps;
    }


    public static File[] GetFiles(String DirectoryPath) {
        File f = new File(DirectoryPath);
        f.mkdirs();
        File[] file = f.listFiles();
        return file;
    }

    public static ArrayList<String> getFileNames(File[] file){
        ArrayList<String> arrayFiles = new ArrayList<String>();
        if (file.length == 0)
            return null;
        else {
            for (int i=0; i<file.length; i++)
                arrayFiles.add(file[i].getName());
        }

        return arrayFiles;
    }

//    public List<PhotoModel> retrieveAllPhotos()
//    {
//        return null;
//    }
//
//    public PhotoModel retrievePhoto(String id)
//    {
//        return null;
//    }
//
//    public boolean editPhoto(String id)
//    {
//        PhotoModel photoModel = retrievePhoto(id);
//
//        return false;
//    }

}
