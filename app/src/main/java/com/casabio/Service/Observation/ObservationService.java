package com.casabio.Service.Observation;

import android.content.Context;

import com.casabio.Model.ObservationEntity;
import com.casabio.Repository.ObservationRepository;
import com.casabio.Service.AbstractService;
import com.casabio.Service.Connection.HttpManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ObservationService extends AbstractService {
	
	public static final String TYPE = "OBSERVATIONSERVICE";
	public static final String OBSERVATION_GET_URL = "http://stage.touchdreams.co.za/CsB/api/v1.0/observations";
	public static final String OBSERVATION_POST = "http://stage.touchdreams.co.za/CsB/api/v1.0/observations";
	public static final String OBSERVATION_DELETE = "http://stage.touchdreams.co.za/CsB/api/v1.0/observations/{nid}";
	public static final String OBSERVATION_GET_ID = "http://stage.touchdreams.co.za/CsB/api/v1.0/observations/{nid}";
	public static final String OBSERVATION_PUT = "http://stage.touchdreams.co.za/CsB/api/v1.0/observations/{nid}";


	protected Context context;

	// GET - "http://stage.touchdreams.co.za/CsB/api/v1.0/observations" - Get Observations
	// POST - "http://stage.touchdreams.co.za/CsB/api/v1.0/observations" - Add Observation
	// DELETE - "http://stage.touchdreams.co.za/CsB/api/v1.0/observations/{nid}"
	// GET - "http://stage.touchdreams.co.za/CsB/api/v1.0/observations/{nid}"
	// PUT - "http://stage.touchdreams.co.za/CsB/api/v1.0/observations/{nid}"
	// GET - "http://stage.touchdreams.co.za/CsB/api/v1.0/observations/{nid}/identifications"



	public ObservationService(Context context){
		this.context = context;
	}

	ObservationRepository observationRepository; //= new ObservationRepository();

	public List<ObservationEntity> getObservations(int id) {
		observationRepository = new ObservationRepository(context);
		//.getEntityAPI(id);

		List<ObservationEntity> response = new ArrayList<ObservationEntity>();
		List<ObservationEntity> observations = new ArrayList<ObservationEntity>();

		String content = HttpManager.getData(OBSERVATION_GET_URL);


		for(ObservationEntity obs :this.parseFeed(content) )
		{
			if(obs.getNid().equals(String.valueOf(id))) {
				response.add(obs);
			}
		}

		return response;
	}

	public static List<ObservationEntity> parseFeed(String content) {

		try {
			JSONArray ar = new JSONArray(content);
			List<ObservationEntity> observationList = new ArrayList<>();

			for (int i = 0; i < ar.length(); i++) {

				JSONObject obj = ar.getJSONObject(i);
				ObservationEntity observation = new ObservationEntity();

				//observation.setId(obj.getString("nid"));
				observation.setVid(obj.getString("vid"));
				observation.setType(obj.getString("type"));
				observation.setLanguage(obj.getString("language"));
				observation.setTitle(obj.getString("title"));
				observation.setUid(obj.getString("nid"));
				observation.setCreated(obj.getString("created"));
				observation.setChanged(obj.getString("changed"));
				observation.setComment(obj.getString("nid"));
				observation.setPromote(obj.getString("promote"));
				observation.setSticky(obj.getString("sticky"));
				observation.setTnid(obj.getString("nid"));
				observation.setTranslate(obj.getString("nid"));
				observation.setUri(obj.getString("photo"));

			}
			return observationList;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}

	}



}
